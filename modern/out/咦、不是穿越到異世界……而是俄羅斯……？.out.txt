え、異世界じゃなくて……ロシア……ですか？
とはるみな
http://ncode.syosetu.com/n4414ff/

從未見識過的光景，從未聽聞過的語言。
原以為自己穿越到異世界的男子高中生，如今轉生成為少女後才發現這裡並不是異世界，而是俄羅斯。
爾後，因父母再婚而移居日本，再婚對象也有個兒子。但是那孩子長相太美型，令人蠢蠢欲動。
這是關於主角想要阻止他成為現充的故事。

小說標籤: R15 校園戀愛 日常 TS 溫馨 精神上的BL 現代 學校

譯者提醒: 請搭配《Boney M - Rasputin》或是《Государственный гимн СССР》一同閱讀

見たこともない光景。銀髪に聞いたことがない言語。
すっかり異世界転生したと思い込んでいた元男子高校生、現少女はある日そこが異世界ではなくロシアであったと知る。
そして親の再婚を機に日本へ移住するのだが、向こうの親にも息子がいて、その息子がとにかく美形で苛立ったので言葉話せないフリしてリア充になるのを阻止してやろうと考える話

－－－－－－－－－－－－－－－
其他名稱：
咦、不是穿越到異世界……而是俄羅斯……？

－－－－－－－－－－－－－－－
貢獻者：章魚子

標籤：R15、TS、node-novel、syosetu、ほのぼの、スクールラブ、俄羅斯、学園、日常、日本、校園戀愛、溫馨、現代、精神的BL有り、高中

－－－－－－－－－－－－－－－
目錄索引：
- 第1章 Глава первая
- - Начинает 序章
- - открытый　発覚
- - папа　爸爸
- - зависть　嫉妬心
- - Свадьба　結婚式
- - инкорпорация　編入
- - Первая школа　初登校
- 第2章 Глава вторая
- - зависимость　依存
- - недоразумение　誤會
- - Средняя школа　中学校
- - Школа вход церемония　入学式
- - Само введение　自己紹介
- - завтрак　朝食
－－－－－－－－－－－－－－－

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝START


＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝CHECK
第00001章：第1章 Глава первая
第1章 Глава первая
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

－－－－－－－－－－－－－－－BEGIN
第00002話：Начинает 序章
Начинает 序章
－－－－－－－－－－－－－－－BODY

『Доброе утрo（早安）』

與那不明白的語言一同，隨著窗簾被拉開，刺眼的陽光喚醒了我的意識。

這裡是什麼地方啊…？

睜眼後所見之處是一個從未見過的房間。

雖然記憶模糊，且與此相前的事情都回想不起來，但至少可以確定這裡不是醫院。

患有先天性心臟疾病的我，過去曾經發生過幾次突然昏倒休克而被送去醫院急救的案例。
所以這次醒來的時候以為自己又昏倒送醫，但瞬間又沒了這個念頭。

出於基本衛生，醫院的牆壁及天花板都是純淨的白色。
然而這裡的天花板上卻並非如此，向上看便一目了然。

如此一來，這裡應該就是某個人的家了吧。
大概是個慷慨大方、心地善良的人吧。
因擔心病危倒下的我，而好心地將我送至自宅之中…不過可以讓我吐槽一點嗎？

為什麼床上會有寶寶專用的音樂吊鈴在轉動啊！？ 一般高中生床上不會出現這種東西吧！！

「…啊……」

原本是以大吼大叫的氣勢發聲，卻只能發出這種絲微的呻吟。

喉嚨壞了嗎？

慌張失措的我，想要就此坐起身子，身體卻紋風不動。
就算是用盡了全身的力氣，也只能使指尖微微的顫抖。

這個情況是不是有點不妙啊？

無法順利的發出聲音，身體也動彈不得。這確實是需要急救的緊急案件。

現在不是在別人家躺著的時候了。
如果再不趕快送去醫院急救，到時候可能狀況會進一步惡化。
然而我卻無法將這件事傳達給任何人。

想必是將我搬到床上的某人沒有想這麼多，認為只要讓我休息足夠，身體便能就此康復。
但如過再照這個樣子下去，我十之八九會因此命喪黃泉。

那位供我住宿、與我素昧平生的善心人士，要是因為我死了，可能會因此深感內疚，隨後受不了精神壓力而倒下……。
還可能最終因此踏上自己的後塵。

只有這件事絕對不能讓它發生。

咕，早知道就去學一些手語了……不對啊，身體又不能動。

正當我感到束手無策時，突然身體浮倒了半空之中。

剛剛正苦思冥想，所以完全沒注意到眼前這位漂亮的銀髮女性將我抱起。
眼睛的顏色呈青色。天藍色的眼眸令人聯想到獵戶座β的參宿七，如同淺灣的海水般明亮。
配上尖而高挺的鼻子，有如妖精般美麗動人，是個打從心底都覺得漂亮的女性。

話說，不要把我抱起來啊……。
即便我有貧弱、瘦小、死矮子這三個綽號體重也有40公斤……才對啊？
被女性如此輕而易舉地抬起，果然還是會很沮喪啊。

『Что такое материя（怎麼了嗎？）』

銀髮的女性用著我聽不懂的語言，輕輕地歪頭說道。
想歪頭的是我這邊吧。這是什麼語言呢……

雖然說我失憶了，前一天發生的事情也是模糊不清，但至少還隱隱約約記得我從未出國去海外。
所以這個人可能是在日外國人……感覺也不太像，那是觀光客囉……？

若真是如此，那還真是給人添麻煩了呢。
原本開心出國旅遊，卻因為我的緣故而使旅行停擺。
……啊，真的很對不起。

……恩？　你想讓我維持這個姿勢然後帶我去哪裡啊…………

「―――！！？」

我顯得有些手足無措，要是我亂動的話，馬上就會被她捏臉頰。不過這麼說好像合情合理。
因為從她後方的鏡子中，倒映出一名銀髮女性高舉一個小嬰兒的光景。

這……是夢嗎……。一定是這樣。沒有其他的可能……。
一股難以忍受的睡意襲來。意識開始朦朧，眼皮有如沉重的巨石搖搖欲墜。

這是夢。是夢的話，睡醒後睜開眼又會看到醫院的天花板了……應該是這樣才對。
但是、如果……沒有看見的話、和現在一樣沒有改變的話……如果說這不是夢的話…………

銀髪的女性。陌生的語言。
我還沒笨到不會解讀這些蛛絲馬跡。我一定是……轉生到了異世界了吧。

===========

(註: Orion Blue也可翻譯為印度藍，十六進制顏色代碼為#a2dcea)


－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00003話：открытый　発覚
открытый　発覚
－－－－－－－－－－－－－－－BODY

姆，所以我應該是轉生到了異世界了吧。
時光匆匆，自從醒來後已經過了七年的歲月。

雖然至今還未曾見識過漫畫和動畫中會出現的異世界魔法或是劍術。
不過經過這麼長一段時間，總算是理解了這邊的語言。

『愛爾菲優斯卡（Альфиюшка），可以過來幫忙我一下嗎？』
『來了ー』

聽見母親的呼喊後，我從家中奪門而出。
我們家位於一個大草原上，整個大草原上只有我們家的風車小屋，附近都沒有鄰居。
所以必須要自行栽培作物、飼養家畜等等，算是過著自給自足的生活。

因此時不時會需要我過去幫忙。

出家門後，在附近的田裡，發現母親懷裡拿著將野菜裝得滿滿的菜籃。
去年為止都只有採收一天會吃的份量而已……是發生了了事什麼導致心境上的變化嗎？

『哇，堆得跟山一樣高』

跑到母親身旁，她對我露出了苦笑。

『…姆ー，也是呢。是時候告訴你了，待會兒有事跟你說』
『是什麼事呀？』

『話說回來，有這麼多野菜的話，愛爾菲優斯卡也能練習烹飪呢』
『誒誒ー……好麻煩喔』

『這樣不行啦，要好好練習烹飪啊……』

母親溫柔地撫摸我的頭，告訴我目前我最不想承認的事實。

『因為愛爾菲優斯卡是女生嘛』

……沒錯，看來我在這個世界的性別是女性。

愛爾菲優斯卡是我的愛稱，本名是愛爾菲婭（Альфия）

是留有一頭遺傳自母親標誌性的銀色長髮的七歲小孩！

……自己這樣說出來總有種莫名的悲傷。

『……別灰心！雖然現在還不擅長烹飪，但只要多練習幾次就會上手了！

　而且我們家愛爾菲優斯卡長得這麼可愛，未來一定可以找到好老公的！』

母親見到我一臉失落後，似乎是誤會了什麼，趕緊為我加油打氣。
但是，這對我來說不是什麼安慰的話語。是追撃啊。而且還是即死攻撃……

跟男人結婚什麼的……我死都不要……

母親在生了我之後馬上就離婚了，所以經常把結婚的話題掛在嘴邊，讓我內心每次都受到不少傷害。
……雖然很想要阻止她繼續這個話題，但其實母親的心情也不是不明白，所以都下不了手。
所以每次都只能支支吾吾地敷衍過去。

『是呢……。但是現在還不考慮看看嘛』

今天也到了這個時候，我就這樣敷衍的回答母親後，迅速地往家中廚房移動。
母親似乎是想繼續說些什麼，但我無法保證一定會對母親所說的話說到做到，所以乾脆就假裝沒聽見。

真心覺得自己是個不乖的女兒……母親，對不起。


『……愛爾菲優斯卡，我有重要的事情要跟你說』

在我們享用完晚餐不久後，母親忽然對我說了這句話。
母親的神情比平常更認真，她一本正經的模樣，不禁使我的腰背挺直。

『有什麼事嗎……媽媽？』
『其實是這樣的……』

應該是原本下午採收時就想說的話吧，似乎是有難言之隱，母親幾度開口卻都沒有說出半句話。

『……媽媽？　沒事吧？　不好開口的話，不用現在說也沒關係喔？』
『…抱歉讓你擔心了，已經沒事了』

母親深呼吸一口氣後，語重心長地說道。

『愛爾菲優斯卡，你仔細聽好。其實我找到了你的新爸爸了』
『呼誒？』

意料之外的話語，使我驚慌失措的發出奇怪的聲音。
誒、是要再婚……的意思嗎？　誒、……誒誒！？

『……我、我的、新爸爸？』

表情凝固的我重複咀嚼著母親剛才所說的話。

『嗯。所以說呢，我們在這個月底之內就要搬家過去了喔。地點在日本（Япония）』

誒，什麼？　……剛才好像聽到了沒聽過的詞彙，那裡是哪裡啊？

『姆，我知道多少會有一些不安，但不會有事的。
會好好教導新的爸爸關於我們的語言和禮儀。
而且，你母親我還會一點點他們的語言呢。
雖然這樣離開祖國（Россия），遠走他鄉是有點寂寞……
但是日本（Япония）也是個好地方喔？』

母親一邊說道，手中緩緩地拿出了一張地圖。應該是這個世界的地圖吧。
可能是心理作用吧，總覺得這跟地球長得非常相似。呀ー這真的跟地球長很像呢。
我說怎麼越看越像……這也太像了吧？　不，這不就是地球啊。

『看好了，這裡就是我們現在居住的地方，俄羅斯（Россия）喔』

母親指著地圖。

誒，那不是俄羅斯嘛。

『然後，我們要去的地方是這裡，日本（Япония）。雖然面積不大，但是有很多人居住在那裡喔』

誒，就是日本啊。
先等一下，訊息來得太過衝擊讓腦袋有點運轉不過來。

意思就是說這裡不是異世界而是俄羅斯，一直以為是在說的異世界語實際上是俄語……我其實是俄羅斯人嘛……！？
……這種事誰知道啊！！沒聽聞過的語言，沒見過的毛髮，加上獨特的自給自足生活方式，一般人都會誤會這是異世界吧！！！

總之這裡並非我想像中的異世界。

而我則是轉生成俄羅斯人了。


－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00004話：папа　爸爸
папа　爸爸
－－－－－－－－－－－－－－－BODY

距離我發覺自己是俄羅斯人一事，已經過了兩周的時間。
太過衝擊的事實，一開始的一星期每晚我都是以淚洗面、哭濕枕頭
現在的話沒有這麼動搖了，能保持平常心……一定……應該吧。

先不說這個。

後來從母親那邊得知，她的再婚對象名字叫神崎拓實。
職業是某大企業底下的一名上班族，家中還有一個孩子，目前為單親家庭。

是母親年輕時與父親到日本旅遊時偶然認識的
從那之後，便從互相連絡開始慢慢培養感情，到現在的決定再婚。

順帶一提，他們兩人似乎是通過手機聯絡彼此。

明明這七年間從來沒有在我面前使用過一次……到底是躲在哪裡使用……

嘛、總而言之，以上這些談情說愛的故事，就是我晚餐正在大快朵頤章魚時從母親口中所聽到的內容。

母親你也太喜歡神崎先生了吧……。
至今以來你把心裡那份好意藏在什麼地方了啊……
不對，反過來想，正因為是要瞞著我，平常一直壓抑著這份情感如今才會大爆發。

即便過了這七年，母親依舊美麗動人。
一想到這麼漂亮的女性就要這樣嫁給神崎先生，生為原男生的我總覺得有些嫉妒。

嗯……現充給我稍微爆炸一下吧？

先不開玩笑，如果神崎先生真如母親所言溫柔待人，那我理當歡迎。
雖然搞得我一副高高在上的樣子，但這是因為有父親這個前車之鑑。為人正直的母親沒有看好男人的眼光。。
所以我才要挺身而出，看清那個男人。為的就是希望這次母親能得到幸福。

『愛爾菲優斯卡（Альфиюшка），睡不著嗎？』

躺在一旁的母親似乎是注意到我遲遲沒有入睡，轉身過來語待溫柔地詢問道。
四周一片漆黑，早已過了平常就寢的時間。

但是，明天終於要離開俄羅斯……回到日本了。
一想到這裡，心臟忽然開始撲通撲通地狂跳，睡都睡不著。

『……嗯』

在母親面前想隱瞞也沒有意義，我老實地點頭承認，母親輕輕地摸著我的頭。
然後微微張嘴，唱歌哄我入眠。

這首歌是……。

『媽媽？』

『還記得嗎？　是搖籃曲喔。雖然最近都沒有唱歌了，但我在愛爾菲優斯卡小時候唱得還不錯喔？』

母親露出懷念的神情。

嗯，我記得喔。我……在這世聽到的第一首歌……。
總是能讓不知所措的我卸下心房……的那首歌。

注意到的時候，急遽的心跳聲早已平息。
眼皮突然變得很重，就這樣意識一點一滴地被睡魔奪走，漸漸進入夢鄉。

『……晚安。媽媽』

『晚安，愛爾菲優斯卡。我愛你』

感受到額頭上觸感的同時，我也失去了意識。


◇


『初次見面，愛爾菲婭醬。我的名字叫神崎拓實。請多多指教？』

『嘁』

『誒……？』

不禁砸嘴，但這出事有因。

現在的日本時間是早上8點，俄羅斯和日本的時差是6個小時，相當於俄羅斯的凌晨2點鐘。
對每天晚上準時9點上床睡覺的我，這是個難熬的時段。

但是為了重要的第一印象，我硬忍著睡意表現出精神的模樣。

此時出現在狼狽不堪的我面前的是，用著一口不流利的俄語與母親侃侃而談的超美型帥哥。
視線另一頭映照出低著頭紅著臉的母親。

作為原男生會砸嘴也不是沒道理。

帥哥是敵人。超級、大敵人。
內心懷疑母親是受騙而來的機率直飆上升。

直直地死盯著他看，神崎先生有點困惑地皺起了眉頭。

『那ー個……愛爾菲婭醬？』

『……有什麼事嗎！』

『……想要吃糖嗎？』

『不需要』

居然想用糖果點心收買我……果然這傢伙是敵人啊！

「啊哈哈……該怎麼辦呢。好像被討厭了呢」

「真的很不好意思。她平常是個很乖的孩子」

「唔ー嗯、究竟是哪裡出了問題呢。要是我再多學一點俄語就好了」

正當我對他更加戒備時，母親和神崎先生開始用日語對話。
大概是以為我聽不懂，所以講得很大聲，在一旁的我都聽得清清楚楚。

「不然我去勸勸愛爾菲優斯卡那孩子？」

「不，沒關係。我想靠自己與那孩子建立信賴關係」

「可是……」

「放心吧。我絕對會與愛爾菲婭醬建立連她親身父母都引以為傲的親子關係！」

……我全都聽得一清二楚喔。

仔細一瞧，神崎先生眼睛下方有厚厚的黑眼圈，一定是為了我們的事情昨，晚期待得輾轉難眠吧。
而且看到他們倆人紅著臉有說有笑的樣子……我怎麼忍心拆散他們。

內心深處我也知道神崎先生是個好人。

說到底本來就是他們兩個人之間的關係，對這樣的他們一味撒嬌也太可笑。
是沒睡飽所以思考才不靈活嘛……我在做什麼啊。

『那個……』

我走到談笑風生的兩人面前，低下了頭。

『誒，怎麼了嘛？』

『母親就拜託您照顧了』

霎時間一陣沉默。
但也只是一瞬間，神崎先生滿面春風地敲了一下自己的胸膛。

『交給我吧』

為什麼只有這句話發音比較正確。這也是傳說中的帥哥補正嘛……。嘛，就交給你了……。
話說回來，好像是因為終於放下心中的大石頭，讓睡意迅速侵蝕我的意識。啊ー，已經是極限了！！

絕對要讓母親幸福喔！就交給你了。

『爸爸』


－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00005話：зависть　嫉妬心
зависть　嫉妬心
－－－－－－－－－－－－－－－BODY

『這裡就是我們的家』

神崎先生帶我們進入一棟兩層式的洋房建築。
庭院比起一般居家還要大一點，除此之外沒什麼特別的地方。

進入屋子內的我四處張望，這時被母親拉了一下手腕。

『愛爾菲優斯卡，你知道嗎？　在日本，入屋內以後都要說一聲「打擾了」才是禮貌』

『誒ー……』

這種事當然我知道，但還是要裝作第一次聽說。
明明沒有接觸過日本卻會說日語，肯定會被當成奇怪的孩子。
今後的生活也會受其影響，可能得裝上好幾年。

「打擾了」

「打……擾……了…」

從母親那邊鸚鵡學舌地試著說了一遍。

……感覺好像有點會太刻意了？　神崎先生在一旁偷笑。
嘛，反正沒有暴露應該也沒關係吧。

『慢慢習慣就好啦。我也沒辦法馬上就把俄語練得滾瓜爛熟呢。
撒，快進來吧。我來介紹我的兒子給你』

我們跟在神崎先生後面，穿過玄關，來到客廳處。
一名少年正坐在沙發上。

『這位就是小犬，響。以後就是愛爾菲婭醬的哥哥』

才一見到他那張嘴臉，又忍不住想要砸嘴。

響是比我早一個月出生的孩子，長相神似語神崎先生
雖然年紀輕輕，但論長相已經宛如一名美少年，這樣形容一點也不為過。

……看久了只會嫉妬心一直作祟……

「響，要跟人家打招呼啊」

「初次見面，我是響」

被用日語打招呼了，所以要先假裝歪頭聽不懂。任何事情一旦決定了，就必須貫徹始終呢。

「喂，響」

「爸爸，對不起。只是想試試看能不能真的用日語溝通……『初次見面，我是響』」

『初次見面，我是愛爾菲婭』

這次是因為用俄語對我打招呼，所以流暢地回敬。
不過不知為什麼，響的臉紅通通的，表情很焦急的樣子。

「……不只是外貌好看，就連聲音都這麼可愛嘛……真狡猾……」

　…………

　……

如果我是真的女生的話早就陷進去了，剛才的那個表情……。
這傢伙果然很危險，未來肯定會成為女性殺手。

多麼令人羨慕……咳咳。過份的傢伙呢。

我絕對要阻止，不能讓這種事發生……但是也不能讓他討厭我……有沒有什麼好辦法呢……。
……總之只要讓他忙到連跟女生打招呼的空閒都沒有就好了吧？

……嗯。我決定到學生時代結束為止都將日語封印。
如此一來，即使不甘願，響也必須作為我的哥哥不厭其煩、時時刻刻的照顧我，讓他忙得不可開交。
意思就是說，可以阻止響成為現充！

抱歉了響。你是無罪的，但我實在無法容忍帥哥受歡迎。
要恨的話，就恨你自己生來就是帥哥吧。

別小看男人的嫉妒心啊！

以這天為界，我開始了每天騷擾響的日常。

◇

神崎先生早已為我和母親準備了房間，我們自我介紹結束後，便帶我們進房間休息。

『那間就是愛爾菲婭醬的房間喔』

我被帶到了二樓的房間盡頭，那裡好像就是我的房間。
另外，隔壁是響的房間。神崎先生和母親的房間則是位於一樓。

嗯、嘛……刻意將孩子和自己的房間樓層錯開……？
畢竟神崎先生和母親兩人都還很年輕…………

新的弟弟或妹妹可能會比想像中還要快出世…………
該用什麼表情祝福他們好呢……開始擔心之後能不能在他們面前假裝沒事，好好地露出微笑。

……這是什麼。

粉紅色、粉紅色、粉紅色。眼睛所見之處皆為粉紅色。已經達到了瘋狂的境界。
打開自己房門的瞬間，我已經被眼前蜂擁而至的粉紅色恐怖畫面震懾到了。

整個人瞠目結舌，說不出半句話。

『怎麼樣，你喜歡嘛？』

神崎先生竟然還問我喜不喜歡這個……？　怎麼可能會喜歡……？　誒，奇怪的人是我嗎……？
往身後一看，母親和響兩人也是一整個傻眼，就說我是正常人吧。

有了作為女兒的大義名分的我，臉上綻放出燦爛的微笑，轉身對神崎先生說道。

『非常討厭』

當然，神崎先生聽到之後錯愕地雙膝跪地。


－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00006話：Свадьба　結婚式
Свадьба　結婚式
－－－－－－－－－－－－－－－BODY

母親與神崎先生的結婚典禮是在那件事之後三個月舉行。

到時候神崎先生和母親就是……不，已經不能稱呼神崎先生，而是義父先生。
總而言之，那一天，他們兩人將正式結為連理。

『恭喜媽媽！打扮得非常漂亮喔』

『謝謝你，愛爾菲優斯卡』

按照流程，主賓致詞，乾杯敬酒，結婚蛋糕入刀儀式後，便開始晚宴，進入賓客交談的時間。

母親一身亮白飄逸的禮服顯得楚楚動人。真的，對能和這麼漂亮的佳人結婚的爸爸心生厭惡。
或許今世成為女人也不見得是壞事。如果是男人的話我都快嫉妒死了。

要幸福啊，你這**。

我用手軸撞了一下被母親緊摟著，一臉色眯眯的義父。

『請務必要讓母親幸福，義父先生。要是讓她不幸，我不會原諒你喔？』

『啊啊，就交給我吧。誠如之前所言，我會讓她幸福的。我答應你』

嗯。嘛，雖然說事已至此，根本沒有懷疑過。
從他與日俱進的俄語中，其中付出的努力，便能聽出他心中所思。

『嗯，謝謝你』

『……話又說回來，其實我也有話要對愛爾菲婭醬說』

不知為何，義父先生忽然板起臉孔，表情嚴肅。
究竟想跟我說什麼呢……。如果是關於新弟弟和妹妹的事，我沒有自信保持微笑啊。

背後冷汗直流。

『是……請問是有什麼事呢？』

『那個、就是……關於我的事情……可不可以不要叫我義父先生，而是……爸……爸爸―――』

『――――喂ー，愛爾菲婭！你過來這邊一下』

義父先生越說越小聲，而響的聲音完全蓋了過去，完全沒聽到他剛才說了什麼。

『不好意思，響。等我一下。……對不起。義父先生，剛才沒聽清楚可以請你在說一遍嗎？』

『―――嗚――。不，沒什麼大不了的。別在意啦』

『？　雖然不太清楚，不過沒什麼事的話，我就先過去響那邊了喔』

那麼響在哪裡呢……找到了！他正被不認識的叔叔阿姨們包圍著，對我招手。
義父先生先前在這邊都用近親相稱，那他們應該是義父先生那邊的親屬吧。

一邊思緒著這件事，一邊朝響的所在跑去。
離開之際，我的視線瞥到垂頭喪氣的義父先生和一旁正安慰他的母親。

……所以說到底是發生了什麼事呢……。


「―――呼呼呼，說的對啊」

「能和你說上話真是太好了。謝謝你了，愛爾菲婭醬」

『能和你說上話真是太好了。謝謝你，愛爾菲婭』

『不，我才是有幸與您談到話。叔叔、阿姨』

「不，我才是有幸與您談到話。叔叔、阿姨」

我對著離去的叔叔阿姨們小小地低頭鞠躬。

與他們聊過才知道，原來這些叔叔阿姨們是義父先生的兄弟姊妹。
嘛，雖然說有聊過，不過也不是直接交談呢。

叔叔他們不會說俄語。
加上我還有不會說日語的設定，全部的會話都是透過響幫我們雙向翻譯而來。

『……吶，愛爾菲婭』

『什麼事？』

『不會覺得不好意思嘛？　』

『……有一點點呢。謝謝你喔，響』

『你呀……唉……慢慢習慣，把日魚記住吧』

從那之後也過了三個月，響的俄語還是很拙劣。這之中應該也造成他很大的負擔。

雖然覺得有些對不起響。但還不能暴露自己會說日語的事。
如果響一旦發現我會說日語，他對我的執著便會消失。
到那時候就完蛋了，有著俊美容貌的響會立刻變成現充吧。

至少到我前世死亡的年齡之前，陪我玩玩這場鬧劇吧。
所以我不能答應響的請求，而作為代替如此回覆道。

『我對努力的，只是……』

『只是？』

『因為有哥哥在，所以不用特別記住也沒關係吧？』

『…………！』

今後也請多多關照了，哥哥。
請竭盡心力全方位的照顧好我，減少自己成為現充的可能性。

在那之後，結婚典禮也順利的如期進行，最終平安落幕。
而與此同時，我的名字也變成了神崎愛爾菲婭。


－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00007話：инкорпорация　編入
инкорпорация　編入
－－－－－－－－－－－－－－－BODY

『明天開始這裡就是愛爾菲婭醬要上的小學喔』

結婚典禮結束後的數天過去，今天義父帶我一起前往明天開始要就讀的國小參觀。

現在是六月，開學典禮已經過了兩個月，所以以編入的形式入學。（註）

義父原本怕我因為語言和環境而不適應這邊的文化，提議延一年再入學。
不過在我強力反對和拼命說服下，得以在今年就編入入學。

我會婉拒延緩一年就讀的原因毋須贅述。
如果參與今年的入學，我和響就是同一年級的學生了。

要是我們不同年級，就很難讓他照顧到我了。
為此，我真的是使出了渾身解數。

具體來說是如何說服的，太害羞了還是不說了。
但也因為這件事讓義父和母親把我貼上兄控這種讓我顏面盡失的標籤……。

順帶一提，我目前還未學會任何一句日語。
……就是在這種設定下，才讓我和響進入同一個班級學習。

理由也很簡單，畢竟會說俄語的人少之又少。
又不像英文或中文主流化，一切就變得理所當然。

雖然有些學校可能會有老師精通一點俄語，但在小學應該沒有這種人物。
而在這之中，唯有響能跟我溝通交流，所以編進同一個班級也是合情合理。

一切都按照我的計畫如期進行。


『啊，愛爾菲婭醬。你看，響來了喔』

『誒』

忽然被喊了名字有點嚇到，慌張的看向義父，他正指著學校的某處。
真如他所說，沿著手指的方向望去，在學校的二樓窗邊看見了響的身影。

現在好像是上課中。響一看就知道他完全沒在聽課，一臉無趣地拖著腮翻開教科書。
前座的男生跟響也是一模一樣的姿勢，但不知為何唯獨響看起來特別詩情畫意…………

…………

……哈！？　不小心就看入迷了。何等的失策啊……！
……不過就算如此，竟然能讓我看到出神……這就是傳說中的帥哥補正嘛……。

帥哥真是不合乎邏輯的存在。可不能放任不管。

我為了進同年級連兄控的名號都背負了。給我做好覺悟吧……哥哥。
絕對不會讓你成為現充。無論如何都不會讓你得逞的。

我在心中默默發誓。


『已經繞了一圈，還想再逛一下校園嗎？』

在學校外圍繞了一圈後，義父回頭向我詢問道。

雖然想繼續逛，但由於我們沒有事先向學校申請，所以不能進入學校內部。最多也只是在外圍再繞一圈罷了。
……那就這樣吧。學校也不是什麼有趣的地方，繞幾圈都一樣，有大致看過一遍就夠了。

『不，這樣就夠了』

『這樣啊。那我們回家吧』

『嗯。謝謝你今天陪我來學校參觀，義父』

『不用客氣啦。為女兒付出一點辛勞，對父親來說是理所當然的職責。
比起這個，愛爾菲婭醬。如果發生了什麼讓你困擾的事，要馬上去找響求助、依靠他喔？』

……誒，剛才說了什麼？　依靠他什麼的……。這是得到父母認可了嘛！！！

得到了大義名份的我，對義父這句話大大的點了頭。

『好的！就算義父不說我也打算這麼做！我會好好依靠響的』

『誒？　啊、……嗯。嘛你明白就好』

從義父得到了許可。我會盡情地依靠你的。

呵呵呵呵呵，明天將會是充滿樂趣的一天。
我開始想像我未來的校園生活，臉上露出邪惡的笑容。


「……看來愛爾菲婭醬心裡頭無法放下響呢。小小年紀還真是精神可嘉呢，我會偷偷支持愛爾菲婭醬的戀愛喔」

『……』

話說回來……好像被嚴重誤會了啊。
那個，義父。至少也用俄語嘟囔吧。雖然我也不會特意去否定就是了……。

終於切身感受到假裝不會說日語的辛勞。


============

註:
此處譯者不將編入譯為轉學，因為在日本編入學與轉學乍看之下為相似之物，然而於教育行政上有很大的差別。
一般轉學指的是未完成學業的情況下，於國內同類型的學校之間轉移，轉學的程序可經由校方辦理學校轉入及轉出手續。
編入學，指的是不同類型的學校內的轉移。一般用於於①國外學校②其他已畢業/不同系所，但已有部分必修學分/學籍(例如專科學校、短期大學等)重新入學
故需要先從原先的學校辦理退學程序(未完成學業者)，再重新辦理個人入學手續。
當然同類型學校也可選擇不辦理轉學，而辦理退學及編入手續，但編入學程序上較為繁瑣。

[quote]「転入學」は編入學と似た言葉のため一般の人には混同されている場合も多いが、教育行政用語としては全く別な意味の言葉である。
転入學とは、國內の同種の學校間を1日も間を空けずに異動することである。
國內の同種の學校であっても1日でも間が空く場合は、前の學校を退學し、新しい學校に編入學したことになる（転入學ではない）。

転入學の場合は學校間での転出・転入の手続きになるが、編入學の場合は當事者が個人で手続きを行うのが一般的である。[/quote]

以上原文及資料源自維基百科

*「義父さん」應譯為義父，會多翻譯個先生為個人譯誤，さん一般的確是尊稱指先生及小姐
但是父さん、母さん此類並未特意將後者譯出，如此翻譯只會顯得太超過


－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00008話：Первая школа　初登校
Первая школа　初登校
－－－－－－－－－－－－－－－BODY

『嗯ー 果然很可愛！』

『謝、謝謝媽媽。那就決定穿這件……』

『這樣不行喔，愛爾菲優斯卡。還沒把全部的衣服都試過呢。下一個試穿這件看看？』

『但是，時間快要……』

『可以試穿一下嗎？』

『是……』

時間來到要入學的當天早晨。
我成了母親的換裝人偶。

今天開始要去上的小學沒有制服規定，服裝可以自由搭配。
所以母親想讓我換上最適合的衣裝，就這樣整整一個小時被強迫換穿衣服。

雖然也轉生了七年的光陰，已經不如從前牴觸穿著女裝生活，但一小時連續試穿簡直是折磨。

女生在選擇衣服的時候都這麼挑剔的嘛……。隨便穿一件襯衫搭長褲不就得了……。
嘛，如果把這句話說出口一定會被說教很久，所以還是留在心裡就好。

在嘆氣之餘，我穿上母親給的白色連衣裙，這時房門口傳來咚咚咚的敲門聲。

『愛爾菲婭醬。時間差不多了，再不出門就會遲到囉？』

是義父的聲音。

望向時鐘，的確現在是響要出門上學的時間。
同時看向時鐘的母親，瞪圓了眼睛，手摀著嘴。

『不會吧，已經這麼晚了？』

『媽媽，不好意思我先出門了喔。響也在等我呢……』

『好吧。原本想讓你試穿各式各樣的衣服的……』

母親將我上上下下審視了一番。
不久便很滿意似地輕輕點了頭，摸了摸我的頭。

『嗯，這件衣服與你非常合襯。路上小心喔！』

『我出門囉！』

在母親的催促下我離開了房間。

話說回來，如果校方沒有給予合格判定該怎麼辦呢。寧願遲到也要換上最好看的衣服嘛？
……雖然覺得校方應該不至於這麼嚴格，卻無法抹除心中的不安。最後的衣裝有得到母親同意真是太好了。

心中一邊思序著這些瑣事，一邊快步向大門口移動。
眼前響已經在玄關處穿好鞋子等我了。

『抱歉，等很久了嗎？』

『不，我也才剛弄好。那我們出發吧？』

『嗯』

……又見面了伙伴。最後一次使用你的時候，從未想過我們還會再次相見。
今世我也從男生變成了女生，而你也從原本的黑色變成了紅色。今後也請多多關照喔。

我揹起放置在玄關處一個嶄新的紅色書包（Ransel），跟在響身後一同出門。

早上走路去上學的學生不多。
話雖如此，距離學校越近，路上的學生也漸漸多了起來。

在來來往往的學生當中，果然髮色與眾不同的我非常顯眼。
每次與學生們擦肩而過，都會屢屢回頭查看。

「那孩子長得真可愛……」

「簡直就像是個人偶」

「和響是什麼關係呢」

對話中馬上就出現響的名字了，可見他在學校中的知名度之高。
在我前世的時候，即使是同班同學都從未記住我的名字……。所以說帥哥是不可原諒的……。

『沒記錯的話，愛爾菲婭應該是要先去教職員辦公室報到吧？』

『好像是這樣沒錯』

『你在生氣嗎，愛爾菲婭？』

『才沒有生氣』

『……是嗎？　嘛怎麼樣都好。知道地點在哪裡嗎？』

『不知道』

『我想也是。從那邊的階梯往上走，一上去就會看到了』

往響所說的方向看去，有個通往二樓的樓梯，上方還有標註前往教職員辦公室的貼紙。
看來我必須先去那邊報到。

『那我就先去教室了喔？』

『嗯，待會兒見』

和響分別後，我朝著教職員辦公室的玄關處前進。
謹慎地一步步登上樓梯，按下門口玄關處的對講機（Interphone）。

過不久，一名似乎是職員，戴著眼鏡的女性打開了門。

「來了ー。哦，你就是愛爾菲婭醬吧。已經聽說過你的事情了。撒，進來吧」

『您在說什麼我聽不懂』

「啊ー，原來如此。原來如此呢。愛爾菲婭醬聽不懂日語。抱歉呢。即便這樣說你也聽不懂吧……。先等我一下喔」

女子從口袋中取出了手機，對著它重複了剛才的話語。
於是，從手機中發出了俄語的聲音。

『你就是愛爾菲婭？　關於你的事情我已經聽說過了。進來談吧』

……原來是手機翻譯軟體呀。

翻譯軟體雖然語法上會給人一種莫名其妙的感覺，但是大致上還是可以理解的。

我心服首肯地點頭同意，聽從她的話跟著進入教職員辦公室。


「好了ー。大家安靜，注意這邊。今天班上會有新同學加入喔！」

從教室中傳來剛才那位戴眼鏡的女性的聲音―――老師的聲音。
她的聲音宏亮到在教室外的我都能聽得一清二楚。

「誒ー！是誰呀？」

「會是什麼樣的人呢……」

受到老師的影響，台下的學生們嘰嘰喳喳地討論不停。


「好了ー，大家安靜一點！那我來介紹一下。從俄羅斯遠道而來的轉學生，愛爾菲婭醬」

老師同時大聲地拍手兩次，作為事前商量好進教室的暗號。
我推開了教室的門。

一陣歡聲雷動的鼓掌叫好。可能沒有預料到新同學會是外國人吧，其中也夾雜著驚訝的聲音。

如果現在能做個自我介紹就好了，但既然是不會說日語的設定，我只能稍微行一下禮。

「大家，愛爾菲婭醬到之前都還居住在俄羅斯，所以還不太熟悉日語，要好好跟她相處喔。
那愛爾菲婭醬你就坐到響後面的座位……啊要把這句話翻譯翻譯……」

老師正要拿出手機翻譯的時候。

響突然站了起來，用俄語對我說道。

『愛爾菲婭的座位在我後面』

他大概是想要幫老師一把才說話的吧。
然而在還沒跟大家解釋關於我們的家庭關係之前，大家會如何看待這樣的舉動呢……。

「誒，響君會說俄語啊？」

「好厲害唷ー……」

「你們兩個人是什麼關係呀？」

「你喜歡她嗎？」

「原來你喜歡她啊ー！」

響說完俄語後，霎時間鴉雀無聲，下一秒取而代之的是教室中同學們的熱烈討論。

最近的小學生好像都有將男女關係良好就歸類是戀人關係的習慣……。

在老師跟大家說明我們的關係之前，我只能一臉裝無辜地說著『什麼？　是什麼意思呢？』，維持演技真的很累人……。

就這樣我的小學生活拉開了序幕。

絕對不會放過你的喔，哥哥。首先是小學六年生涯。全靠你了喔。


－－－－－－－－－－－－－－－END


＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝CHECK
第00009章：第2章 Глава вторая
第2章 Глава вторая
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

－－－－－－－－－－－－－－－BEGIN
第00010話：зависимость　依存
зависимость　依存
－－－－－－－－－－－－－－－BODY

我經常想起那天所發生的事。
令人難忘的六年前的春天，父親將再婚的對象與她的女兒帶進了我們家。

俄羅斯人的義母和義妹成為我的家人，距今也過了六年的時間。

當時我七歲，而我今年已經十三歲了。
等過了這個春假，我就要升為中學生了。

身高也比以前高了一點，身材也漸漸變得更成熟。

這樣的我最近有個煩惱。

那就是―――我的義妹愛爾菲婭完全沒有心思學習日語。

進我們家也相處了六年。一般人記性再差，至少也會幾句日常用語。
然而我們家的愛爾菲婭卻完全做不到。讀書寫字上雖然沒有問題，但就是不會口說。

……她會變成這樣，可能都是我的錯。

小學六年間，我因為一直受到愛爾菲婭的依賴而感到快感
幾乎是片刻不離左右，無微不至地照顧她。

無論她和誰說話，我都會在一旁幫她翻譯。

在日本生活，必須會說日語。
為了讓她也能說上流利的日語，也曾經為了這件事狠狠地責備過她

『有響在我身邊就沒有問題！』

她卻這樣一臉得意地回答我。

老實說，愛爾菲婭太依賴我了。
換句話說，就是我平常太嬌慣她了。
我顧慮不周的行為只會妨礙她成長的機會。

「我該怎麼辦呢……」

要如何讓她開口說日語呢。
這便是我的煩惱。

也不能一字一句都寫下來溝通，只要她不會說日語就不能自立。

無法不再依賴我。

「……依賴我的時候跟她告白交往也沒有用啊……」

愛和依賴是全然不同的東西。

現在這個狀態下，即便跟愛爾菲婭告白，她大概也會同意跟我交往吧。但她那份依存並非情侶間的戀慕之情。
而且要是哪天我不在的話，就麻煩了。

如果能和愛爾菲婭交往我一定會很開心，但倘若不是兩情相悅，就不能成立這種關係。

告白就等到她不再依賴我，從我這邊畢業之後吧。

「……真想早點跟她告白」

我不禁嘆氣道。

第一次見到她的時候就覺得她很可愛，這幾年間她又變得更加漂亮了。
她的一舉一動，一顰一笑，每次都輕意地奪走我的心。

病態般光滑白晰的肌膚，一頭長至腰部的銀髮，碧藍清澈的眼眸。
雖然前凸後翹，但整體上來說苗條勻稱的身材。
如同義母，有著令人聯想到妖精般傾城的美貌。

除了我以外還有許多人也被她的姿色所吸引，其中也有很多男生是以成為她的男朋友為目標。
光是我認識的就已經超過兩隻手指頭能數過來的人數，而且這個數字隨著她年齡成長不斷增加。

競爭對手逐年增加，搞得我焦躁不安。


「……只剩下這個辦法了嘛……」


實際用嘴巴說出來以後，內心更加堅定了。
所以我決定再次說出口。

「為了告白……今年必須和愛爾菲婭保持距離！」

『響？』

「誒？」

身後傳來已經聽慣的嬌柔女聲，回頭一看，身穿睡衣的愛爾菲婭一臉好奇地站在我後面。
才剛洗完澡的她伴隨著熱氣滿臉通紅，看上去超可愛的……啊，這不是重點。

『那、那個……愛爾菲婭？　你什麼時候站在那裡的……？』

『才剛到而已喔……』

『不是啦、剛才說的那些話……那個……』

以為誰都不在，所以才一個人在客廳下決心說出口，沒想到螳螂捕蟬，黃雀在後。

被聽到告白的事情了！？　而且還是那個要告白的對象。

就在我快要羞愧地無地自容的時候，愛爾菲婭說了一句話讓我冷靜了下來。

『剛才說的那句話是什麼意思呀？　因為聽不懂日語，所以不理解呢。你說了什麼嗎？』

好危險……得救了……。

幸好當初是用日語下決心。忽然覺得愛爾菲婭不懂日語真是不幸中的大幸。

『沒什麼啦。只是試著喊了一下今年想完成的抱負罷了。讀書和運動都要加油這樣的』

我手忙腳亂地在她面前比手畫腳，愛爾菲婭則是一副理解的樣子『原來如此』地點頭說道。

『姆ー。是這樣啊，那好好加油吧』

『愛爾菲婭你不要把這件事置身度外。今年也要好好地學習日語―――』

『沒關係的。聽說讀寫，除了說之外，其他我都得心應手。
而且今年也會和響同班。請多多指教囉，哥哥』

見到愛爾菲婭那燦爛的微笑差點就要點頭示弱，但是我忍下來了。

今年一定要讓她學會說日語。

『不好意思，我今年不會在你身旁時時刻刻幫著你了』

此話一出，愛爾菲婭驚訝地瞪大了眼睛。

『……為什麼？』

『沒什麼特別的理由』

死氣沉沉的氣氛。

先打破沉默的，是愛爾菲婭。

『……我明白了。響也很忙的吧』

『不好意思啊』

『沒關係的。那我先回房間了。時間很晚了，差不多該睡覺了，晚安，響』

『嗯。晚安』

愛爾菲婭離去後，我全身無力地躺在後面的沙發上。。


太好了，把要說的話一口氣都說出來了。

雖然和愛爾菲婭保持拒離難免有些寂寞，但這是為了我們未來交往的試練之一，無論如何都必須忍住。


……請你快點學會日語吧，愛爾菲婭。


－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00011話：недоразумение　誤會
недоразумение　誤會
－－－－－－－－－－－－－－－BODY

『……』


回到房間後的我關上房門，上了門鎖後，瞬間全身無力，就這樣跌坐到了地上。
我取下用笑容掩飾的假面，內心隱藏的情感滿溢而出。

『騙人……的吧……』

腦袋像是壞掉的收音機一樣，不斷重複播放剛才與響的對話。他突然拒絕我了。

『……說是要和我保持距離……到底是什麼意思啊，響……』

雖然小聲的抱怨了一下，但其中的緣由自己還是很清楚的。

那是在我去客廳的時候，意外聽到他下決心的那句話。

他那時是用日語說的，當下只能假裝聽不懂，不過響他確實親口說了…

「為了告白……今年必須和愛爾菲婭保持距離」這句話。

……得到了這樣的情報還能不懂嘛，我可沒那麼遲鈍。

響他啊……一定是喜歡上誰了。已經對一直學不會日語卻還死纏爛打的我感到厭煩了。

也不是什麼值得大驚小怪的事。響從今年開始就是中學生了。
正好邁入青春期的階段。想和誰談情說愛也不奇怪的年紀。

依照我的推理，應該是想引起愛慕對象的注意，才沒有時間陪在我身邊吧。
簡單來說，就是因為我會妨礙到他的戀愛，所以被他狠狠拋棄了……

『唔…………他究竟是喜歡上誰了呀……』

這件事把我弄得心煩意亂。

因為那可是超級大帥哥的響耶？　行為舉止大方溫柔，待人又和藹可親，那個無論是外表還是內在都是滿分的響？
女子人氣投票之中，那個以一票不差獨得第一名的響？

被那樣的響進行愛情攻勢的話，沒有一個女生不會墜入情網的吧。嗯，應該沒有這號人物存在。
至少和我們同一所小學的女生全部都會被他迷得神魂顛倒。這點我能拍胸保證。

……可惡，這樣下去他不就是直直通往現充的未來嘛……

一想到響要和其他人交往，內心深處就隱隱作痛。

對於不習慣的心痛，雖然片刻之間無法心領神會，不過我馬上就得出了答案。

……說的也是呢。如此一來，我這六年來的處心機慮全都將付諸東流。
已經沒有意義繼續假裝不會說日語讓他照顧我了，會心痛也是理所當然。


………不行…。

在房間內來來回回走了好幾遍，想盡各種能阻撓他的計策，可惜實在得不出個好法子。

如果能用日語對話，那麼不懂日語的我就不再是個阻礙。
但是六年來我在家中扮演不會說日語的角色也已經定型
事到如今，也不能裝作一夕之間就忽然學會用日語說話，這樣只會戳破我長年埋下的謊言。

……如果當初有聽響的忠告，慢慢地一字一句學就好了。

現在後悔也來不及了。

時間無情地流逝。

回過神來，才發現時針已經指向凌晨三、四點之間，大家早就都睡著了吧。
只聽得見自己的呼吸聲和時鐘轉動所發出的無機質聲。

……已經有些心神疲倦，而且熬夜對皮膚不好。

『……這樣就只能使出那個手段了……嘛……』

經過長時間思考得出的答案，我鐵了心並催促自己下去執行。


這個方法可說是把雙刃劍。
因此，如果條件允許，我盡可能不使用。
要是在幾年之前就知道響喜歡上別人的話，我肯定不會採取這個手段。

但是六年的歲月太過沉重。
事已至此，如今已經不能後退。
只能不擇手段了。

為此，我終於下了決心。

……無論你說什麼都不會離開……我絕對不會離開的。

都已經靠著男人的氣魄跟你耗到這般田地。作為原男人，絕對不會讓你成為現充。

之後會比以往更加靠近距離，做好覺悟吧。
作為家人，被呵護的對象……不，我將作為一個女人執著於響，如此覺悟。

好在我也有幾分姿色。
除了繼承母親姣好的面容，身材上凹凸有致，可說是理想的體型。
至少同年齡上沒有人是我的對手。

而且原本是男生的我，更了解男性的心理。

只要能靈活運用這兩項優勢，無論對手是誰，我一定能讓響拜倒在我的石榴裙下。

等著瞧吧響。我絕對會讓你回頭。你會後悔當初決定和我保持距離。

【讓響徹底愛上我，使他眼裡都容不下其他女人】

這就是我的作戰計畫。

『嘛，我會讓你死心塌地的愛上我的……不過以後就不需要再演不會說日文語了嘛……
……之後一點一滴的慢慢說出來吧…………這麼晚睡皮膚會不會變差呀……』

不知為何我心情舒暢許多，漸漸閉上雙眼，沉入夢鄉。


－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00012話：Средняя школа　中学校
Средняя школа　中学校
－－－－－－－－－－－－－－－BODY

叮――叮鈴叮鈴叮鈴叮鈴叮鈴！

忽然一陣鬧鈴聲響起，把我從睡夢中驚醒。

距離上次被鬧鈴叫醒是什麼時候呢。

至少在上小學的時候，每天早上響都會在鬧鈴響起之前就來叫我起床，所以幾乎都沒實際使用過。

『就算如此，入學典禮也應該來叫醒我嘛……』

雖然早就預料到他今天大概不會來叫醒我，但還是忍不住發了一下牢騷。

我按下耳邊響個不停的鬧鐘，伸了個大大的懶腰後起身下床。

脫去身上印有波卡圓點的睡衣，拿取要換上的水手服和迷你裙。

『唉……沒想到要換上這麼羞恥的衣服……』

輕輕地嘆了一口氣後，仔細地在鏡子面前檢查自己的服裝儀容。

『嗯，頭髮沒有睡亂的痕跡！衣服表面整齊乾淨！身上沒有半點污漬！微笑OK！今天的我也很完美！』

在鏡子前華麗地轉了一圈後，自信滿滿地指著鏡子中的自己說道。
隨後拿起床上的書包，快步離開房間。


『早安，愛爾菲優斯卡。今天比平常晚一點呢』

『早安媽媽。這其中有很多緣由啦――啊……』

與母親互道早安時，眼角瞥到坐在餐桌正享用吐司的響
他見到我後，面有難色的別過臉去，我則是若無其事的朝他旁邊的座位坐下。

『早、早安，愛爾菲婭。今天真是美好的早晨呢……』

『嗯，早安響。托你的服，今天早上被久違的鬧鈴聲吵醒，才能讓我切身感受到早晨的美好呢。
下次如果你隔天沒有要來叫我，至少前一天晚上可以先跟我說一下吧』

『……的確是這樣呢。應該先跟你知會一聲的，抱歉』

『如果你從明天開始天天叫我起床的話，也不是不能原諒你喔』

『抱歉，這我做不到。吃完早餐就趕快出門吧。我會在外面先等著』


『來了喔，愛爾菲優斯卡。這是今天的早餐。畢竟已經是這個時間了……要吃快一點喔――怎麼生氣了呀？』

『才沒有生氣』

『誒，和響發生了什麼事嗎？』

『我才不認識那種傢伙。我吃飽了，先出門了』

一口氣飲盡杯子裡的牛奶，嘴巴叼著吐司，手裡拿著書包便走出了客廳。

『什！？　不可以一邊嘴巴叼著麵包一邊走路。喂！要好好吞下去再走啊――』

身後傳來母親苦口婆心的勸告，但我充耳不聞、頭也不回地跑向玄關處。
迅速穿上放在那裡全新的樂福皮鞋（Loafer）後奪門而出。


今天的天氣是萬里無雲的大晴天。

全身沐浴在春天和煦的陽光下，門口處響坐在自行車上等著我。

今天開始要去的中學與小學不同，離家裡稍微有點距離。
所以家人允許使用自行車通勤。

我悶不吭聲的默默把叼著的吐司吃完，拉出了自己的自行車，解開防盜車鎖，跨坐上車。
等到我確認完東西都準備齊全後，響才開始踏進腳踏板。

『這樣下去時間可能會來不及，稍微加快腳步吧』

『……知道了』

我們騎了一會兒後，響對我這樣說道後開始加速。

為了追上加速後的響，我也使勁踏著腳踏板前進。

春風輕輕地拂面而過，沁人心脾。


我在這一世是第二次踩自行車。

有了前世經驗的我，第一次騎便駕輕就熟，所以認為沒有再練習的必要。

正因為如此。也沒有什麼其他特別的意圖。

只是、單純想要感受更多涼風、那個……不知不覺就想站著騎……

在加速的狀態下，穿著迷你裙的我就這樣從座墊上站起。

接下來發生的慘劇應該也不用我多說……霎時間裙子飛了起來。

『啊……』

『誒……』

『啊、那個……抱歉』

我趕緊慌張地坐下，騎在旁邊的響就像是已經看見了我的裙底風光，整個人面紅耳赤。

嗯，的確這樣成功的讓他更加在意我，但是……

這不是我希望的形式啊……！！

只不過是下面的一塊布被看到而已……為什麼我會這麼動搖啊……

一股前所未有的羞恥感油然而生，害我到學校前都不敢直視響的臉，沉浸於羞愧之中。


－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00013話：Школа вход церемония　入学式
Школа вход церемония　入学式
－－－－－－－－－－－－－－－BODY

『………………』

『………………』

經過剛才的意外，我們彼此不看對方，互不交談，默默地騎自行車。

登上斜坡後，終於看見學校。

「你是新生對吧」

「對。我是新生」

「那旁邊銀髮的這位呢？」

「啊，她跟我一樣也是新生」

「這樣啊。那麼請把自行車停放在那邊的停車場，然後往體育館移動」

在響離開服務中心後的數十秒後。

『……響，你們剛才說了什麼？』

『……他叫我們把自行車停在那邊的停車場，然後去體育館』

『……嘿ー』

……好尷尬。

費盡心思才讓自己忘掉剛才發生的事情，打算故作鎮定地詢問響在服務處談話的內容
卻還是太過害羞，根本無法看他的臉。

……真不像平常的我，我到底怎麼了呀……誰來告訴我……

然而，沒人能回答我所提出的疑問……

結果我們眼神完全沒有交會，對話就此結束。
就這樣在沒有化解尷尬的氣氛下，兩人前往體育館參加入學典禮。


「哦，這不是響君和愛爾菲婭醬嘛ー」

「好久不見～。畢業典禮以也只過了一個月而已吧？」

「最近都過的怎麼樣～？」

「好久不見。還好喔。話說回來，這些隊伍的排列是依照什麼標準呢？」

「同一個小學排成四列。按順序排就可以了吧」

一進到體育館，便發現有很多熟悉的面孔。
不用說，他們就是與我們同一所小學的同學們。

話雖如此……可能是因為畢業典禮結束的關係吧。
僅僅大約一個月的時間沒有碰面，卻又和暑假等長假那種長期沒見面的感覺不同，總覺得很懷念。。

但是我並不會因此沉浸於傷感之中，而是死死盯著和響說話的女孩子們。

那個騙走了響的心的傢伙可能就藏在這些人之中……

一想到這，就更不能讓響離開我的視線。

我絕對不會把響讓給任何人！無論是誰我都不會認輸的！

「啊ー 請大家安靜」

我像是要做開戰宣言一樣，對靠近響的女生們一個個用眼神示意
此時台上出現了一位西裝筆挺的男老師。

那名老師在台上咳了幾聲，藉此請底下學生們安靜並開口道。

「那麼，現在開始進行入學典禮―――」


入學典禮進行的很順利，圓滿的結束了。
結束後，馬上就公布了學生們的班級分配。

對在場的學生全都發配了一份印好的表單。
我在確認自己和響都是被分配到同樣的二班後，鬆了一口氣。

雖然事前已經從義父那邊聽說會把我們分配在同一班，心裡還是有點不安。

……才想著太好了，就看見以前和我們同小學的女生們，也跟我們進了同一個班級。

總共有五名。
現在還不能確信那名對象是否在他們五人之中，不過既然都是同班同學，以後就可以嚴密監控她們的一舉一動。

大家都確認過班級後，接下來每班的班導老師開始帶著自己班上的學生去以後要上課的教室。
一年級的教室都在學校的一樓，班級按照簡單的方式排列，從走廊開始算起分別是一班、二班、三班、四班、五班、六班。

「你看……那個孩子……」

「銀髪…………長的好可愛…」

踏入教室後，從四面八方傳來驚訝不已的聲音。

他們都是跟我就讀不同小學的學生。大家紛紛將視線集中在我身上

嘛，畢竟是銀色的頭髮。第一次見到可能會很吃驚吧。

很久沒看到像他們這種充滿新鮮感的反應，雖然我沒有表現在臉上，但其實內心面露苦笑。

我乖乖地按照黑板上填寫的座位表入座。


不久後，吵雜聲平息，所有學生都入座，班導老師開始向我們說明學校的規範和注意事項。


－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00014話：Само введение　自己紹介
Само введение　自己紹介
－－－－－－－－－－－－－－－BODY

班導師簡單說明完後，接下來帶我們參觀校園。

目的好像是帶我們實際走訪以後開學後會使用的特殊教室。

帶我們去大致上看了一下音樂教室和美術教室，接著繞了校園一圈才回到原本的教室。

我們坐回原本的位置入座，等待之時，班導老師走上講台。
老師見到牆壁上的時鐘後便開口說道。

「誒ー……今天預定的事項大致上都已經完成，可是我們還有一點時間，那我們就來進行自我介紹吧」

此話一出，台下的學生們便開始議論紛紛，然後一個個對我露出閃閃發光的眼神。
幾乎是全班的同學都這樣。甚至還有同學特意把座椅轉過來就為了望向我。

唔ー你們不會做的太明顯了呀？至少也含蓄一點，偷偷瞥向我就好了呀……。
雖然說如果同班同學是外國人，應該會很在意沒錯。

還有班上一部分的男生。不要再趁亂對我使用充滿熱情的眼神暗送秋波，我要揍人了喔。
喂、不是、我說你不要誤會啊！不是叫你對我飛眼傳情的意思！

只見他一臉放蕩的眼神試圖勾引我，我一臉不耐煩的眼睛半開
結果不知為何他反而變本加厲，對我眨眼示意，害我慌張的移開視線。
此時老師終於大聲地拍手吸引大家的注意。

「好了好了，各位同學們。我知道你們對人家很好奇，之後會讓你們再追加提問的。
　 　我們今天時間有限，趕快先來進行自我介紹吧」

一瞬間還以為得救了結果原來不是。只是被延後而已。

……也、也罷。但不好意思辜負了你們的期待，我還不能說日語。
等你們聽完無法理解的俄語自我介紹後，應該就不會有什麼勇者還對我有興趣提問了吧。

「那我們請1號同學開始自我介紹」
「是。我是從――小學來的，淺野隆太。請多多指教」

正當我還在思考時，自我介紹已經開始了。

「有沒有人想問更多關於淺野君的事情呢？」
「有。誒ー斗，請問淺野君喜歡吃的食物是什麼呢？」
「是咖哩飯」
「還有其他人嗎？……OK，那請淺野君就座。我們有請下一位」
「是！」

被叫到號碼的學生先起立，做個簡單的自我介紹後，再讓其他同學提問題。
如果回答完沒有追加問題後便坐下換下一位，就是像這樣的流程。

每位同學最多被提三個問題，所以每個都最多數十秒就結束，眼看下一個就要輪到我了。

「OK。請這位同學入座。――那麼有請下一位」

雖然叫到我了但我仍假裝不懂。不會日語的設定才不會因此而有所動搖。
沉默了數十幾秒後，周圍同學見我毫無反應，而開始嘰嘰喳喳的討論起來。此時響終於挺身而出為我翻譯。

『愛爾菲婭，輪到你的自我介紹了』
『誒？自我介紹？』
『恩，報上自己的名字也好，至少說句話』
『我知道了』

我緩緩起身，望向周圍的同學們輕輕的行了一個禮。
慢慢一字一句的說出口。

『我是神崎愛爾菲婭。請多多指教』

說完再次行禮，內心嘲笑著那些同學們啞口無言的嘴臉，並慢慢坐下。――

『……啊，先等一下，愛爾菲婭。同學們還沒提問呢』
『誒？』
「各位有沒有什麼想問愛爾菲婭的問題？有的話我可以幫忙做翻譯」

才剛說完，同學們各各不約而同地奮力舉起手。

不對呀，響。你不是想跟我保持距離嗎？
如果又過來幫我翻譯，就會跟小學時代一樣被人當作總是跟我在一起的標籤，你這是到底打著什麼如意算盤……？
有點搞不懂響在想些什麼。

「請問喜歡吃什麼食物呢？」
『喜歡吃什麼食物？』
『和食』
「她說喜歡吃和食」
「請問興趣是什麼呢？」
『興趣是什麼呢？』
『音樂鑑賞』
「她說音樂鑑賞」
「請問有喜歡的對象嗎？」
『你、你有喜歡的對象嗎？』
『沒有』
「…沒有……她是這麼說的」

不我說，即便有喜歡的對象，也沒有笨蛋會蠢到直接在這邊說吧。

數分鐘內，接二連三的回答問題。班上同學們的提問都輪過一遍後，老師宣布告一段落，我這才終於坐下。

為什麼只有我被問了其他人十倍之多的問題呀……

正當我百思不解時，響忽然站起身，開始自我介紹。

……都是因為響的緣故我才受難，要是你們也這樣問題轟炸響就好了……

我的願望成真了，對響的提問比我好幾倍。已經輪完第二輪的提問都還沒問完。其中主要都是關於我的提問。
……心情暢快多了。
全班同學都自我介紹完後，便結束今天的行程，各自踏上歸途。

=============

作者備註:
響）
通過同學們提問，可以從中得知平常不敢對愛爾菲婭開口問的事情＞保持距離


－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00015話：завтрак　朝食
завтрак　朝食
－－－－－－－－－－－－－－－BODY

入學典禮的隔天。
今天是我的值班日，所以起的比平常早。
下樓後打開廚房的門，見到了母親的身影。

『早安，愛爾菲優斯卡』

『早安，媽媽。今天的早餐要做什麼呢？』

『和平常一樣，荷包蛋和烤土司就好』

『知道了』

打開了衣櫃，穿上圍裙手腳俐落地準備要使用的食材。

這種值班制度是從我小學五年級開始。
遵循母親的方針『女性在結婚前至少要學會下廚』，一周一次早上會輪到我做早餐。

老實說，成為女生已經有十三年之久。
或許是受到身體影響，經常會單純因為一件小事而生氣或悲傷。雖然不排除精神上有可能幼齡化的傾向，卻從來沒有對男性抱持過好感。
究其原因，除了響和義父之外，我從未和其他男性聊天談話。

所以結婚的可能性一丁點也沒有。會簡單做一些料理，但沒必要繼續提升烹飪技能。
直到剛才為止我都是這麼認為。

『――誒？媽媽，剛才說了什麼嗎？』

把雞蛋打入加過食用油的的平底鍋內，正當蛋白有點微焦時，母親忽然開口說了一句話。
聽完她所說的話，使我不禁弄錯了小火和大火，手忙腳亂地關了瓦斯，母親又再次複述。

『有喜歡的人了吧？』

『怎、怎麼這麼突然！？才沒有呢！』

我可沒有說謊。沒有喜歡的對象，倒是有個想要讓對方徹底迷戀上我的人。

『不用害羞也沒關係喔？對了，作為女性的前輩就給你兩項建議吧』

但是，母親卻將我的說詞輕描淡寫地帶過，一本正經地向我說道。

『第一項。如果對方是一個喜歡誇大其辭、自吹自擂的男人，那麼勸你就此打住吧……嘛，雖然愛爾菲優斯卡喜歡的他是個穩重可靠的人，這方面應該不成問題吧』

『在說誰啊！？在媽媽眼裡我究竟是喜歡上誰了呀！？』

『恩恩，別害羞別害羞』

『人家才沒有害羞！』

自說自話，根本無法交談。

『呼呼。那接下來就給你第二項建議吧。男人很單純，想要抓住他的心，最重要的就是要先抓住他的胃』

『……』

細細品味其言，的確有一番道理在。

雖然母親誤會我喜歡上某個人了，但是我想讓特定的某人喜歡上我也是事實。

無論如何，反正每周都必須要下一次廚。那拿出努力向上的態度，認真求進步不是更好嘛……？

如果不只外在美，連內在都能達到完美無缺……屆時，響就沒有不瘋狂迷戀我的理由了。

說不定之後甚至還會懇求我過來幫忙煮菜呢。

『呼呼呼』

想像著不遠的未來可能會發生的這一幕，我自然地揚起了嘴角。

我才不會這麼簡單就放過你。跟我保持距離這個決定，我會讓你後悔莫及。

說曹操，曹操就到。響一邊伸著懶腰一邊走了過來。

『早安母親……愛爾菲婭？原來今天輪到你值日啊』

見到我的時候一瞬間愣住了，但下一秒又理解似地頻頻點頭並朝餐桌入座。

――啊。我的荷包蛋，還在平底鍋上煎著呢。

『抱歉喔，響。再稍等我一下就好』

我趕緊準備好餐盤，將烤好的吐司和煎好的荷包蛋，盛到響的餐盤之中。
隨後也盛好母親和自己的那一份。

荷包蛋和烤土司。雖然現在只能在餐桌上端上幾道簡單的料裡，但有朝一日，等我的廚藝精湛後，到時候天天擺滿漢全席，餐桌上都會是琳瑯滿目的佳餚。

此時想到了一句適合這個場面用的話語。

『祝用餐愉快，開動吧』


－－－－－－－－－－－－－－－END

